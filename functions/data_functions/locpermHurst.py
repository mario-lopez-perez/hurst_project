import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from functions.data_functions.Hurst_fit import *
from functions.data_functions.locpermRS import * 
 
def locpermHurst(avincrements, N, nperm, blocklen, dfname, sec2av):
    LongsList, av_RS = LocPermRS(avincrements, N, nperm, blocklen) #Calculate lengths of subseries and corresponding averaged RS statistcs for nperm local permutations of blocklen-length blocks of each subseries of N trading days of dataset avincrements (see locpermRS.py).
    Days = np.arange(len(avincrements[:, 0])) #Trading days
    locpermHurst = np.zeros((len(Days[: -N]), nperm))
    for d in Days[: -N]:
        for k in range(nperm):
            ind = [i for i in range(len(av_RS[d, k, :])) if ~np.isnan(av_RS[d, k, i])]
            if len(ind) > 3: #This is to remove averaged RS statistics fundamentally consisiting of nan values
                popt, pcov = curve_fit(H_fit, LongsList[d, k, :], av_RS[d, k, :]) #here we fit a power law to the asymptotic averaged RS statistics of the k-th permutation of the d-th sunseries of N trading days; popt are the parameters of the regresion, pcov will not be used
                locpermHurst[d, k] = popt[1] #The H parameter of the fit (see Hurst_fit.py)
            else:
                locpermHurst[d, k] = np.nan
    columns = ['perm_{}'.format(i) for i in range(nperm)]
    pd.DataFrame(data = locpermHurst, columns = columns).to_csv('output_csv/{}_Hurst_L{}_av{}_perm{}_locperm{}.csv'.format(dfname, N, sec2av, nperm, blocklen))  