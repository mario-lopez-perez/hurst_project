from numba import njit, prange
import numpy as np


@njit(parallel = True)
def LocPermRS(avincrements, N, nperm, blocklen): #This function takes as input avincrements, the array of logarithm returns of prices of an asset, N, the length in trading days of the subseries whose Hurst exponent is being calculated, nperm, the number of shufflings wich will be performed to calculate the locally randomized sample of Hurst exponents H_perm, and blocklen, the length of the local blocks to be permuted. It is written in this way in order to parallelize with numba. It returns Longlist, the list of lengths of subseries whose average RS statistics is being calculated in order, and locpermRS, the array of those average RS statistics. 
    Days = np.arange(len(avincrements[:, 0])) #The index for the trading days.
    permRS = np.zeros((len(Days[: -N]), nperm, 10)) #The array of average RS statistics of locally shuffled subseries
    LongsList = np.zeros((len(Days[: -N]), nperm, 10)) #The array of lengths of those subseries.
    for d in prange(len(Days[: -N])):
        DaySeries = avincrements[ d: d+ N, :].flatten() #Subseries of N trading days of returns.
        for k in range(nperm): #This is the loop for each of nperm local permutations
            for j in range(0, len(DaySeries) - blocklen, blocklen): #This is the loop over blocks of blocklen elements
                np.random.seed(j + k) #The seed is to make this code deterministic
                np.random.shuffle(DaySeries[j: j + blocklen]) #Shuffle the series of length blocklen
            DaySeries1 = DaySeries[~np.isnan(DaySeries)] #Remove nan values
            L = np.array([9 + i*(int(np.log2(len(DaySeries1))) - 9)/ 9 for i in range(0,10) ])
            for i in range(len(L)):
                LongsList[d, k, i] = int(2**L[i])
                l = int(2**L[i]) #One of the lengths to be used
                RS0 = np.zeros((len(DaySeries1)- l + 1)) #RS statistics for that specific length l
                for n in range(len(DaySeries1)- l + 1):
                    Y = DaySeries1[n: n + l]
                    m = np.mean(Y)
                    sM = 0
                    sm = 0
                    s = 0
                    for x in Y:
                        s += x - m
                        if sM < s:
                            sM = s
                        if sm > s:
                            sm = s
                    RS0[n] = (sM - sm)/np.std(Y)
                permRS[d, k, i] = np.nanmean(RS0) #Average RS statistics for k-th local permutation (shuffling) of d-th subseries of length l
    return [LongsList, permRS]