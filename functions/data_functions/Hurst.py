import numpy as np
import pandas as pd
from functions.data_functions.avRS import *
from functions.data_functions.Hurst_fit import *

def Hurst(avincrements, N, dfname, sec2av): #For a given array of increments of averaged logarithms of prices, returns the array H of Hurst indices of its subseries of N trading days.
    Days = np.arange(len(avincrements[:, 0])) #Trading day
    Hurst = np.zeros(len(Days[: -N])) #This array will containt the Hurst exponents of the subseries of N trading days
    for d in Days[: -N]:
        DaySeries = avincrements[ d: d+ N, :].flatten() #The d-th series of N trading days
        DaySeries = DaySeries[~np.isnan(DaySeries)] #remove nan values
        Longs = np.array([int(k) for k in np.logspace(9, int(np.log2(len(DaySeries))), num = 10, base = 2 )]) #The lengths of subseries whose average RS statistics is going to be calculated (see paper)
        av_RS = avRS(DaySeries, Longs) #The average RS statistics (see avRS.py)
        Hurst[d] = Hurst_fit(Longs, av_RS) #Hurst exponent of d-th subseries of N trading days
    
    pd.DataFrame(data = Hurst.reshape(-1, 1), columns = ['no_perm']).to_csv('output_csv/{}_Hurst_L{}_av{}_noperm.csv'.format(dfname, N, sec2av))  