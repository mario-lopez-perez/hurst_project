import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from functions.data_functions.Hurst_fit import *
from functions.data_functions.permRS import * 
 
def permHurst(avincrements, N, nperm, dfname, sec2av):
    LongsList, av_RS = PermRS(avincrements, N, nperm) #Calculate lengths of subseries and corresponding averaged RS statistcs for nperm permutations of each subseries of N trading days of dataset avincrements (see permRS.py).
    Days = np.arange(len(avincrements[:, 0])) #Trading days
    permHurst = np.zeros((len(Days[: -N]), nperm))
    for d in Days[: -N]:
        for k in range(nperm):
            ind = [i for i in range(len(av_RS[d, k, :])) if ~np.isnan(av_RS[d, k, i])]
            if len(ind) > 3: #This is to remove averaged RS statistics fundamentally consisiting of nan values
                popt, pcov = curve_fit(H_fit, LongsList[d, k, :], av_RS[d, k, :]) #here we fit a power law to the asymptotic averaged RS statistics of the k-th permutation of the d-th sunseries of N trading days; popt are the parameters of the regresion, pcov will not be used
                permHurst[d, k] = popt[1] #The H parameter of the fit (see Hurst_fit.py)
            else:
                permHurst[d, k] = np.nan
    columns = ['perm_{}'.format(i) for i in range(nperm)]
    pd.DataFrame(data = permHurst, columns = columns).to_csv('output_csv/{}_Hurst_L{}_av{}_perm{}.csv'.format(dfname, N, sec2av, nperm))  