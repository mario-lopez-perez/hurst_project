from numba import njit
import numpy as np

@njit('f8(f8[:])')
def RS(X): #This functions calculate the RS statistcs for a time series X.
    #X = X[~np.isnan(X)] 
    m = np.mean(X)
    sM = 0
    sm = 0
    s = 0
    for x in X:
        s += x - m
        if sM < s:
            sM = s
        if sm > s:
            sm = s
    return (sM - sm)/np.std(X)
