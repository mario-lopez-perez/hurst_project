from numba import njit, prange
import numpy as np



@njit(parallel = True)
def avarray(meanprice, tradefreq, sec2av): #the new dataframe averaged in blocks of sec2av seconds 
    A = np.zeros((len(meanprice[:, 0]), len(range(0, len(meanprice[0, :]) - sec2av, sec2av))))
    for i in prange( len(meanprice[:, 0])):
        for j in range(len(range(0, len(meanprice[0, :]) - sec2av, sec2av))):
            if np.nansum(tradefreq[i, j*sec2av: (j + 1)*sec2av]) != 0:
                A[i, j] = np.nansum( meanprice[i, j*sec2av: (j + 1)*sec2av]*tradefreq[i, j*sec2av: (j+1)*sec2av] )/ np.nansum(tradefreq[i, j*sec2av: (j + 1)*sec2av]) 
            else:
                A[i, j] = np.nan
    return A