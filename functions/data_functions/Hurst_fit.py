import numpy as np
from scipy.optimize import curve_fit

def H_fit(t, c, H): #A power map to be fitted in Hurst_fit to the series of averaged RS statistics to get Hurst exponent.
    return c*t**H

def Hurst_fit(Longs, av_RS): #THis functions return the Hurst exponent given by fitting a power map to the averaged RS statistics av_RS of a time series corresponding to the set of subseries lengths Longs.
    ind = [i for i in range(len(av_RS)) if ~np.isnan(av_RS[i])]
    if len(ind) > 5: #This is to filtrate those averaged RS statistics fundamentally consisting on nans
        popt, pcov = curve_fit(H_fit, Longs, av_RS) #popt are the parameters, pcov is no needed
        Hurst = popt[1] #The parameter H of H_fit, that is, the exponent of the power law.
    else:
        Hurst = np.nan
    return Hurst
