
import numpy as np

def indices_I(H, permH, locpermH, q1 = 0.1): #This function calculate the inefficiency/antipersistency indices I_d and I_f defined in the paper.
    permH1 = np.nanquantile(permH, q1, axis = 1) #Get q1 quantile of the sample H_perm of nperm randomized Hurst exponents for each subseries of N trading days
    locpermHm = np.nanmean(locpermH, axis = 1) #Get mean of the sample H_locperm of nperm locally randomized Hurst exponents for each subseries of N trading days

    n_strong = np.count_nonzero(locpermHm < permH1) 
    n_weak = np.count_nonzero(H < permH1)
    l = len(H)
    I_d = n_weak / l
    I_f = n_strong / l

    return I_d, I_f