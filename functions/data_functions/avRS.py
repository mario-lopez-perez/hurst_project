from numba import njit
import numpy as np

@njit        
def avRS(X, Longs):
    av_RS = np.zeros((len(Longs)))
    for i in range(len(Longs)):
        l = Longs[i]
        RS0 = np.zeros((len(X)- l + 1))
        for n in range(len(X)- l + 1):
            Y = X[n: n + l]
            m = np.mean(Y)
            sM = 0
            sm = 0
            s = 0
            for x in Y:
                s += x - m
                if sM < s:
                    sM = s
                if sm > s:
                    sm = s
            RS0[n] = (sM - sm)/np.std(Y)
        av_RS[i] = np.nanmean(RS0) 
    return av_RS