import numpy as np
from math import factorial as fac

def C(n, k):
    return fac(n) / (fac(k)*fac(n-k))

def p_values(H, permH, locpermH, N, q): #This function calculate the p-values P^q(n_d) and P^q(n_f) defined in the paper.
    permH_disjoint = permH[:: N, :] #Take disjoint subseries, for the null hypothesis concerns independent experiments
    locpermH_disjoint = locpermH[:: N, :]# Take disjoint subseries, for the null hypothesis concerns independent experiments
    N_disjoint = permH_disjoint.shape[0] #Number of disjoint subseries of N trading days
    permH_q_disjoint = np.nanquantile(permH_disjoint, q, axis = 1) #q quantile of the sample
    locpermHm_disjoint = np.nanmean(locpermH_disjoint, axis = 1) #mean of the sample
    Hurst_disjoint = H[:: N] #Take disjoint subseries, for the null hypothesis concerns independent experiments


    nq_strong_disjoint = np.count_nonzero(locpermHm_disjoint < permH_q_disjoint) #Number of subseries not satisfying strong null hypothesis 
    nq_weak_disjoint = np.count_nonzero(Hurst_disjoint < permH_q_disjoint) #Number of subseries not satisfying weak null hypothesis  


    Pq_strong = np.nansum(np.array([C(N_disjoint, i)*2**(-N_disjoint) for i in range(nq_strong_disjoint, N_disjoint+1)])) #Strong p-value as defined in the paper.
    Pq_weak = np.nansum(np.array([C(N_disjoint, i)*2**(-N_disjoint) for i in range(nq_weak_disjoint, N_disjoint+1)])) #Weak p-value as defined in the paper.

    return Pq_strong, Pq_weak
