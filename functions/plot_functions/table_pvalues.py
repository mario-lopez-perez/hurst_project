from matplotlib import pyplot as plt

def plot_table_P(Market, table_P, Labels, columns_P, N, sec2av, nperm, blocklen, q3, q4): #This functions plot the table with the previously calculated inefficiency/antipersistency p-values (table_I) for the market Market with stocks Labels, for thresholds q3 and q4. The other parameters are as in main_Hurst.py
    if Market == 'US':
        fig, ax1 = plt.subplots(figsize = (12, 8))
    elif Market == 'MX':
        fig, ax1 = plt.subplots(figsize = (12, 12))

    #gs = gridspec.GridSpec(1, 10)
    #ax = fig.add_subplot(gs[:, 1:])

    # hide axes
    fig.patch.set_visible(False)


    #ax1 = plt.subplot(2,1,1)
    ax1.axis('off')
    ax1.axis('tight')

    ax1.table(cellText = table_P, rowLabels = Labels, colLabels = columns_P, loc = 'center', cellLoc = 'center', colColours = ['cornflowerblue', 'cornflowerblue', 'cornflowerblue', 'cornflowerblue'], alpha = 0.3, colWidths = [0.15,0.15,0.15,0.15])


    #if Market == 'US':
    #    ax1.set_title(r'$P^q(n)$ for $\tau$ = {}, N = {}'.format(sec2av, N))
    #elif Market == 'MX':
    #    ax1.set_title(r'$P^q(n)$ for $\tau$ = {}, N = {}'.format(30*sec2av, N))

    fig.tight_layout()

    plt.savefig('output_png/{}_HurstTableP_L{}_av{}_perm{}_locperm{}_q{}_{}.png'.format(Market, N, sec2av, nperm, blocklen, q3, q4))
    plt.close()
