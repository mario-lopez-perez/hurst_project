from matplotlib import pyplot as plt

def plot_table_I(Market, table_I, Labels, columns_I, N, sec2av, nperm, blocklen, q1): #This functions plot the table with the previously calculated inefficiency/antipersistency indices (table_I) for the market Market with stocks Labels, for threshold q1. The other parameters are as in main_Hurst.py
    if Market == 'US':
        fig, ax1 = plt.subplots(figsize = (8, 12))
    elif Market == 'MX':
        fig, ax1 = plt.subplots(figsize = (6, 13))

    #gs = gridspec.GridSpec(1, 10)
    #ax = fig.add_subplot(gs[:, 1:])

    # hide axes
    fig.patch.set_visible(False)


    #ax1 = plt.subplot(2,1,1)
    ax1.axis('off')
    ax1.axis('tight')

    TableI = ax1.table(cellText = table_I, rowLabels = Labels, colLabels = columns_I, loc = 'center', cellLoc = 'center', colWidths = [0.15,0.15], colColours = ['cornflowerblue', 'cornflowerblue'], alpha = 0.3)#, fontsize = 30)

    TableI.auto_set_font_size(False)


    if Market == 'US':
        TableI.scale(2, 2)
        TableI.set_fontsize(17)
    elif Market == 'MX':
        TableI.set_fontsize(10)
        TableI.scale(1.5, 1.5)

    fig.tight_layout()

    plt.savefig('output_png/{}_HurstTableI_L{}_av{}_perm{}_locperm{}_q{}.png'.format(Market, N, sec2av, nperm, blocklen, q1))
    plt.close()