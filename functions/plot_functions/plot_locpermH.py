from matplotlib import pyplot as plt
import numpy as np

def plot_locpermH(dfname, H, locpermH, sec2av, N, nperm, blocklen, q1 = 0.1, q2 = 0.9): #Plot the evolution of Hurst exponent and q1 and q2 quantiles of locpermH for subseries of N trading days of sec2av returns of averages of logarithms of mean prices. Saves the png file in output_png.
    if q1 > q2:
        return 0
    L = np.arange(len(H))
    locpermH1 = np.quantile(locpermH, q1, axis = 1) #Get q1 quantile of the sample H_perm of nperm randomized Hurst exponents for each subseries of N trading days
    locpermH2 = np.quantile(locpermH, q2, axis = 1) #Get q2 quantile of the sample H_perm of nperm randomized Hurst exponents for each subseries of N trading days    
    plt.subplots(figsize=(12, 9))
    plt.plot(L, H, label = r'$H_{stock}$', linewidth = 3)
    plt.fill_between(L, locpermH1, locpermH2, color='crimson', alpha=0.3, label = r'$H_{locperm}$')
    #plt.title('Evolución del exponente de Hurst y cuantiles 0.1 y 0.9 de \n las series localmente aleatorizadas de ' + str(N) + ' días')
    plt.xlabel(r'Day', fontsize = 20)
    plt.xticks(L[::50], fontsize = 20)        
    plt.ylabel(r'Hurst exponent', fontsize = 20)
    plt.yticks(np.linspace(0,1,6), fontsize = 20)
    plt.ylim(0, 1)
    plt.legend(fontsize = 30)
    plt.tight_layout(pad = 0)
    plt.savefig('output_png/{}_Hurst_L{}_av{}_perm{}_locperm{}_q{}_{}.jpg'.format(dfname, N, sec2av, nperm, blocklen, q1, q2))
    plt.close()