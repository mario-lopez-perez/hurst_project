from matplotlib import pyplot as plt
import numpy as np

def plot_permH(dfname, H, permH, sec2av, N, nperm, q1 = 0.1, q2 = 0.9): #Plot the evolution of Hurst exponent and q1 and q2 quantiles of permH for subseries of N trading days of sec2av returns of averages of logarithms of mean prices. Saves the png file in output_png.
    if q1 > q2:
        return 0
    L = np.arange(len(H))
    permH1 = np.quantile(permH, q1, axis = 1) #Get q1 quantile of the sample H_perm of nperm randomized Hurst exponents for each subseries of N trading days
    permH2 = np.quantile(permH, q2, axis = 1) #Get q2 quantile of the sample H_perm of nperm randomized Hurst exponents for each subseries of N trading days    
    plt.subplots(figsize=(12, 9))
    plt.plot(L, H, label = r'$H_{stock}$', linewidth = 3)
    plt.fill_between(L, permH1, permH2, color='b', alpha=0.3, label = r'$H_{perm}$')
    plt.xlabel(r'Day', fontsize = 20)
    plt.xticks(L[::50], fontsize = 20)       
    plt.ylabel(r'Hurst exponent', fontsize = 20)
    plt.yticks(np.linspace(0,1,6), fontsize = 20)
    plt.ylim(0, 1)
    plt.legend(fontsize = 30)
    plt.tight_layout(pad = 0)
    plt.savefig('output_png/{}_Hurst_L{}_av{}_perm{}_q{}_{}.jpg'.format(dfname, N, sec2av, nperm, q1, q2))
    plt.close()