import pandas as pd
import numpy as np
import glob
import os
from functions.data_functions.average_data import *
from functions.data_functions.Hurst import *
from functions.data_functions.permHurst import *
from functions.data_functions.locpermHurst import *
from functions.plot_functions.plot_permH import *
from functions.plot_functions.plot_locpermH import *
from functions.data_functions.indices import *
from functions.data_functions.p_values import *
from functions.plot_functions.table_indices import *
from functions.plot_functions.table_pvalues import *

Market = 'US'#MX (Mexico) or US (United States), the market under study.
#listfile_meanpr = glob.glob('data/{}/Mean_price/*.csv'.format(Market)) #The list containing the paths to import all the csv files with mean logarithms of prices
listfile_meanpr = ['data/{}/Mean_price/{}.csv'.format(Market, dfname) for dfname in ['F', 'ABT']] #We use this sublist of two assets, F and ABT, in order to produce the data and figures just for this subset of the US Market. If you want to do the same for all the assets of any Market, comment this line and uncomment the previous one.
sec2av = 5 #The number of seconds to average the logarithms of mean prices. IMPORTANT: THE DATA FOR THE MEXICAN MARKET IS ALREADY AVERAGED FOR 30 SECONDS, THAT IS, TO REPRODUCE THE RESULTS OF THE PAPER YOU MUST SET sec2av EQUAL TO 1, AND THUS GET 30 SECONDS AVERAGES, AND N TO 30 TO OBTAIN SUBSERIES OF 30 DAYS.  
N = 5 #The length in trading days of the subseries whose Hurst index will be calculated
nperm = 100 #The number of permutations to carry out over N-days series in to obtain randomized and locally randomized Hurst exponents
blocklen = 300 #The size of the blocks to shuffle when locally randomizing data
q1 = 0.1 #Lower quantil of H_perm and H_locperm to be plotted with the evolution of Hurst exponent, also used as threshold to measure I_d and I_f
q2 = 0.9 #Upper quantil of H_perm and H_locperm to be plotted with the evolution of Hurst exponent

q3 = 0.1 #Quantil of H_perm and H_locperm to be used as threshold when measuring p-values P^q(n_d) and P^q(n_f)
q4 = 0.5 #Quantil of H_perm and H_locperm to be used as threshold when measuring p-values P^q(n_d) and P^q(n_f)

####This block is included in order to quickly precompile the numba function (see permRS.py and locpermRS.py)
arr = np.arange(40000).reshape(10,4000)
a, b = PermRS(arr, 3, 10) 
a, b = LocPermRS(arr, 3, 10, 100) 
#####

Labels = [] #This will be the list of codes of assets
columns_I = [r'$I_d$', r'$I_f$'] #This is the list of columns labels for the table of indices of inefficiency/antpersistency
columns_P = [r'$q = {}, n = n_d$'.format(q3), r'$q = {}, n = n_d$'.format(q4), r'$q = {}, n = n_f$'.format(q3), r'$q = {}, n = n_f$'.format(q4)] #This is the list of columns labels for the table of p-values of inefficiency/antpersistency
table_I = np.zeros((len(listfile_meanpr) + 1, len(columns_I))) #This will contain the data for the table of indices of inefficiency/antpersistency
table_P = np.zeros((len(listfile_meanpr) + 1, len(columns_P))) #This will contain the data for the table of p-values of inefficiency/antpersistency
sum_d = 0 #This wil be the sum of weak inefficiency indices across the market, will be used to calculate the market average
sum_f = 0 #This wil be the sum of strong inefficiency indices across the market, will be used to calculate the market average
sumP1_d = 0 #This wil be the sum of weak inefficiency p-values across the market with quantile q3, will be used to calculate the market average
sumP1_f = 0 #This wil be the sum of strong inefficiency p-values across the market with quantile q3, will be used to calculate the market average
sumP2_d = 0 #This wil be the sum of weak inefficiency p-values across the market with quantile q4, will be used to calculate the market average
sumP2_f = 0 #This wil be the sum of strong inefficiency p-values across the market with quantile q4, will be used to calculate the market average
for i in range(len(listfile_meanpr)): 
    dfpath = listfile_meanpr[i] #The path to the file of mean logarithms of prices
    dfname = os.path.splitext(os.path.basename(dfpath))[0] #The name of the given asset
    Labels.append(dfname)

    ############################THIS BLOCK CARRIES OUT THE HEAVIEST CALCULATIONS IN THIS CODE, THAT IS WHY YOU SHOULD COMMENT IT AFTER COMPILING IT ONCE, AND ACCESS THE RESULTS THROUGH THE CSV FILES PRODUCED BY IT
    meanprice = pd.read_csv(dfpath)  #The actual dataset with mean logarithms of prices
    tradefreq = pd.read_csv('data/'+ Market + '/Trading_frequency/{}.csv'.format(dfname)) #the dataset containing number of transactions per second of the given asset    
    avdata = avarray(np.array(meanprice.iloc[:, 1: ]), np.array(tradefreq.iloc[:, 1: ]), sec2av) #Take the original dataset and average it in blocks of sec2av seconds; the erasing of the first column of the datasets is beacuse it is the column of row labels
    avincrements = avdata[:, 1: ] - avdata[:, : -1] #The inctements of averaged dataset
  
    Hurst(avincrements = avincrements, N = N, dfname = dfname, sec2av = sec2av) #The array of Hurst indices of avincrements subseries of N trading days is calculated and saved to a csv file, this is done in order to avoid repetition of heavy calculations. Once compiled for the first time, this line should be commented and the data accesed through that csv file as below.
    permHurst(avincrements = avincrements, N = N, dfname = dfname, sec2av = sec2av, nperm = nperm) #The array of Hurst indices of avincrements shuffled subseries of N trading days is calculated and saved to a csv file, this is done in order to avoid repetition of heavy calculations. Once compiled for the first time, this line should be commented and the data accesed through that csv file as below.
    locpermHurst(avincrements, N, nperm, blocklen, dfname, sec2av) #The array of Hurst indices of avincrements locally shuffled blocklen-lengthed blocks of subseries of N trading days is calculated and saved to a csv file, this is done in order to avoid repetition of heavy calculations. Once compiled for the first time, this line should be commented and the data accesed through that csv file as below.
    ##################################END OF THE BLOCK TO BE COMMENTED

    H = pd.read_csv('output_csv/{}_Hurst_L{}_av{}_noperm.csv'.format(dfname, N, sec2av)) #Load from csv file the array of Hurst indices of avincrements shuffeld subseries of N trading days previously calculated. This is done in order to avoid repetition of heavy calculations. Once compiled for the first time, this line should be used to access this data, and the lines where that csv file was produced should be commented.
    permH = pd.read_csv('output_csv/{}_Hurst_L{}_av{}_perm{}.csv'.format(dfname, N, sec2av, nperm))#Load from csv file the array of Hurst indices of avincrements subseries of N trading days previously calculated. This is done in order to avoid repetition of heavy calculations. Once compiled for the first time, this line should be used to access this data, and the lines where that csv file was produced should be commented.
    locpermH = pd.read_csv('output_csv/{}_Hurst_L{}_av{}_perm{}_locperm{}.csv'.format(dfname, N, sec2av, nperm, blocklen)) #Load from csv file the array of Hurst indices of avincrements locally shuffled blocklen-lengthed blocks of subseries of N trading days previously calculated. This is done in order to avoid repetition of heavy calculations. Once compiled for the first time, this line should be used to access this data, and the lines where that csv file was produced should be commented.
    H = np.array(H.iloc[:, 1]) #Remove row labels column and convert to numpy array
    permH = np.array(permH.iloc[:, 1:]) #Remove row labels column and convert to numpy array
    locpermH = np.array(locpermH.iloc[:, 1:]) #Remove row labels column and convert to numpy array
 
    plot_permH(dfname, H, permH, sec2av, N, nperm, q1 = q1, q2 = q2) #Plot the evolution of Hurst exponent and q1 and q2 quantiles of permH for subseries of N trading days of sec2av returns of averages of logarithms of mean prices. Saves the png file in output_png.

    plot_locpermH(dfname, H, locpermH, sec2av, N, nperm, blocklen, q1 = q1, q2 = q2) #Plot the evolution of Hurst exponent and q1 and q2 quantiles of locpermH for subseries of N trading days of sec2av returns of averages of logarithms of mean prices. Saves the png file in output_png.

    I_d, I_f = indices_I(H = H, permH = permH, locpermH = locpermH, q1 = q1) #This function calculate the inefficiency/antipersistency indices I_d and I_f defined in the paper.
    P1_d, P1_f = p_values(H = H, permH = permH, locpermH = locpermH, N = N, q = q3) #This function calculate the p-values P^q(n_d) and P^q(n_f) defined in the paper.
    P2_d, P2_f = p_values(H = H, permH = permH, locpermH = locpermH, N = N, q = q4) #This function calculate the p-values P^q(n_d) and P^q(n_f) defined in the paper.

    table_I[i, :] = ["%.2f" % I_d, "%.2f" % I_f]
    table_P[i, :] = ['{:.2E}'.format(P1_d), '{:.2E}'.format(P2_d), '{:.2E}'.format(P1_f), '{:.2E}'.format(P2_f)]

    sum_d += I_d
    sum_f += I_f
    sumP1_d += P1_d
    sumP1_f += P1_f
    sumP2_d += P2_d
    sumP2_f += P2_f

mean_d = sum_d / len(listfile_meanpr) #Mean of I_d across the market
mean_f = sum_f / len(listfile_meanpr) #Mean of I_d across the market
meanP1_d = sumP1_d / len(listfile_meanpr) #Mean of P^q3(n_d)_d across the market
meanP1_f = sumP1_f / len(listfile_meanpr) #Mean of P^q3(n_f)_d across the market
meanP2_d = sumP2_d / len(listfile_meanpr) #Mean of P^q4(n_d)_d across the market
meanP2_f = sumP2_f / len(listfile_meanpr) #Mean of P^q4(n_f)_d across the market

table_I[-1 ,:] = ["%.2f" % mean_d, "%.2f" % mean_f]
#valuesP[-1, :] = ["%.9f" % meanP1_d, "%.9f" % meanP5_d, "%.9f" % meanP1_f, "%.9f" % meanP5_f]
table_P[-1 ,:] = ['{:.2E}'.format(meanP1_d), '{:.2E}'.format(meanP2_d), '{:.2E}'.format(meanP1_f), '{:.2E}'.format(meanP2_f)]

Labels = Labels + ['Average ' + Market] #Add the last row label for the average values of indices and p-values in the tables

plot_table_I(Market = Market, table_I = table_I, Labels = Labels, columns_I = columns_I, N = N, sec2av = sec2av, nperm = nperm, blocklen = blocklen, q1 = q1) #This functions plots the table with the previously calculated inefficiency/antipersistency indices (table_I) for the market Market with stocks Labels, for threshold q1.
plot_table_P(Market = Market, table_P = table_P, Labels = Labels, columns_P = columns_P, N = N, sec2av = sec2av, nperm = nperm, blocklen = blocklen, q3 = q3, q4 = q4) #This functions plots the table with the previously calculated inefficiency/antipersistency p-values (table_P) for the market Market with stocks Labels, for thresholds q3 and q4. 

####THE FOLLOWING IS MATERIAL WHICH DID NOT APPEAR IN THE PAPER. IT IS A LITTLE CODE TO FIT AUTO-REGRESIVE MODELS TO HURST EXPONENT DYNAMICS.